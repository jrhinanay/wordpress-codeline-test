<?php 

/**
 * Template Name: Film Page
 *
 */

wp_head(); ?>

<div id="primary" class="content-area col-sm-12 col-md-12">
<main id="main" class="site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>
        
        <?php get_template_part( 'content', 'page' ); ?>
        
        <div class="container">
            <div class="row">
                <label>Country:</label>
                <text ></text>
                
            </div>
            <div class="row">
                <label>Genre:</label>
                <text ></text>
                
            </div>
            <div class="row">
                <label>Ticket Price:</label>
                <text ><?php echo get_post_meta($post->ID, 'ticket-price', true)?></tex>
                
            </div>
            <div class="row">
            <label >Release Date:</label>
            <text ><?php echo get_post_meta($post->ID, 'release-date', true)?></text>
                
            </div>
        </div>
      
        <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || '0' != get_comments_number() ) :
                comments_template();
            endif;
        ?>

    <?php endwhile; // end of the loop. ?>

</main><!-- #main -->
</div><!-- #primary -->

<?php wp_footer(); ?>