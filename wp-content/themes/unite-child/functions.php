<?php 

// code to inherit css style of parent theme
add_action( 'wp_enqueue_scripts', 'unite_inherit_parent_style' );
function unite_inherit_parent_style() {

    $parent_style = 'unite-style'; 

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

// code to create a post type dedicated for film.
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'test_film',
      array(
        'labels' => array(
          'name' => __( 'Films' ),
          'singular_name' => __( 'Film' ),
          'add_new' => 'Add New',
          'add_new_item' => 'Add New Film',
          'edit' => 'Edit',
          'edit_item' => 'Edit Film',
          'new_item' => 'New Film',
          'view' => 'View',
          'view_item' => 'View Film',
          'search_items' => 'Search Film',
          'not_found' => 'No Film found',
          'not_found_in_trash' => 'No Film found in Trash',
          'parent' => 'Parent Film'
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'products'),
        'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
        'taxonomies' => array( 'Genre','Country','Year', 'Actor' ),
      )
    );
  }

  // code to add custom field in 1 meta box in the admin area.
  add_action( 'add_meta_boxes', 'create_custom_field' );
  function create_custom_field() {
    add_meta_box(
      'film-meta', // $id
      'Film Info', // $title
      'custom_field_film', // $callback
      'test_film', // $screen
      'normal', // $context
      'high' // $priority
    );

  }

// a callback function to display custom fields.
function custom_field_film()
{
  global $post;

  if(!$post){
    ?>
    <label for="ticket-price">Ticket Price:</label>
    <input type="number" step="any" name="ticket-price" id="ticket-price" />
    <label for="release-date">Release Date:</label>
    <input type="date" name="release-date" id="release-date" />
    <?php 
  }else{
    ?>
    <label for="ticket-price">Ticket Price:</label>
    <input type="text" step="any" name="ticket-price" id="ticket-price" value="<?php echo get_post_meta($post->ID, 'ticket-price', true); ?>"/>
    <label for="release-date">Release Date:</label>
    <input type="date" name="release-date" id="release-date" value="<?php echo get_post_meta($post->ID, 'release-date', true); ?>"/>
    <?php 
  }
       
}


  //code to integrate saving/updating of post's custom fields when update button is pressed.
  add_action('save_post', 'save_custom_meta');
  function save_custom_meta($post_id) {
  
    // Validation: to be sure that the post type of the custom field is under test film
    if ('test_film' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
        } elseif (!current_user_can('edit_post', $post_id)) {
            return $post_id;
    }  

    // loop through fields and save the data
    $customFields = [array('id' => 'ticket-price'), 
                    array('id'=> 'release-date')];
    foreach ($customFields as $field) {
      $old = get_post_meta($post_id, $field['id'], true);
      $new = $_POST[$field['id']];
      if ($new && $new != $old) {
          update_post_meta($post_id, $field['id'], $new);
      } elseif ('' == $new && $old) {
          delete_post_meta($post_id, $field['id'], $old);
      }
    }


}

//a code to create show_film shortcode. This will display last 5 list of all shortcode film 
add_shortcode( 'show_films', 'show_films' );
  function show_films( ){
    global $post;
    
    // filter posts
    $posts_array = get_posts( array('post_type' => 'test_film', 
                                      'numberposts' => 5, 
                                      'orderby'=> 'release-date',
                                      'order' => 'DESC') );
    
    //format and display film post info
    ?>
    <?php foreach( $posts_array as $post ) : setup_postdata($post); ?>
    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <ul>
      <li>Country:</li>
      <li>Genre:</li>
      <li>Ticket Price:<?php echo get_post_meta($post->ID, 'ticket-price', true); ?></li>
      <li>Release Date:<?php echo get_post_meta($post->ID, 'release-date', true); ?></li>
      
    </ul>
    <?php endforeach; 
    
   
    wp_reset_postdata(); 
   
   
    
  }
  

?>