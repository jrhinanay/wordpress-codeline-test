<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_codeline_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'L<0+H|x_J b!Xd_oGw~LvD|l1AU38D@xh%YxQ.51OHi38v{jqlRA09eZ^`iQ_(&7');
define('SECURE_AUTH_KEY',  'ucUtj_,F0Wv+Ex?6|rea#>2 /NaPdd?)wajyG( I5yCEBE+0z.BRE)bT7_.wvM=s');
define('LOGGED_IN_KEY',    'AAB:E{jiFC4`-.sp6jCD@F%QT_6vM~JWFTc1a}Y1XIt61nVHmi~H&._I[_ou 4nQ');
define('NONCE_KEY',        'E|>gk^`Tm[;-Ip2NN>5(qNGbitYT3Uk<#a2=F4wXW5v_|t.=d+:8y(C7!uf}>_q+');
define('AUTH_SALT',        '.2O94WE_`*I!l.MAVj68G?Gbw;dJ&0(>7T89F@V6>sqUUP[xd>UzNkhCC nps`qh');
define('SECURE_AUTH_SALT', '(B^8]]JkXFDFt;iNc_6UO,`?VdayFb|>}&9Od3w8E79*RC:U$Oknb1.CDQ$iP1pg');
define('LOGGED_IN_SALT',   '1o6E6[H5%iDJd3qKT8LtvnDx2yIR*gv><&2$v})Ptk&KEnua]uK)$LbiNo}JOm@=');
define('NONCE_SALT',       'ui?*J?fYj,}|qY$~}M qL)1&1(MM5ZMaz~6tc9h@2&WYqcvJBrAg22qVh<^)n#Zf');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'tbl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
